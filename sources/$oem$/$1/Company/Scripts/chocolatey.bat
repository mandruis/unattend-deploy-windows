"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
choco install notepadplusplus.install -y
choco install googlechrome -y
choco install office365proplus -y
choco install firefox -y
choco install flashplayerplugin -y
choco install vlc -y
choco install 7zip.install -y
@echo off

REM Get Computer Manufacturer
FOR /F "tokens=2 delims='='" %%A in ('wmic ComputerSystem Get Manufacturer /value') do SET manufacturer=%%A

IF "%manufacturer%"=="Microsoft Corporation" (
    ECHO Microsoft Machine
)

IF "%manufacturer%"=="Dell Inc." (
    ECHO Dell Machine
    choco install dellcommandupdate -y
)

IF "%manufacturer%"=="LENOVO" (
    ECHO Lenovo Machine
    choco install lenovo-thinkvantage-system-update -y
)
FOR /F "tokens=2 delims= " %%A in ('WMIC PATH Win32_VideoController GET Name /value') do SET VideoProcessor=%%A
IF "%VideoProcessor%"=="Quadro" (
    ECHO NVIDIA Quadro
	choco install geforce-driver -y
)
IF "%VideoProcessor%"=="GeForce" (
    ECHO NVIDIA Geforce
	choco install geforce-driver -y
)
IF "%VideoProcessor%"=="HD" (
    ECHO NVIDIA Quadro
	choco install intel-dsa -y
)