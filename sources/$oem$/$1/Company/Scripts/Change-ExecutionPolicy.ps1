﻿<#  
================================================================================
========= Introduction to Change-ExecutionPolicy.ps1 =======================================
================================================================================
 Name: Change-ExecutionPolicy
 Purpose: Allow User/Administrator a way to execute multi-line scripts
 Author: Dan Stolts - dstolts@microsoft.com - http://ITProGuru.com
 Syntax/Execution: Copy portion of script you want to use and paste into PowerShell (or ISE)  
 Description: Shows Multiple ways to change PowerShell ExecutionPolicy
 Disclaimer: Use at your own Risk!  See details at ITProGuru.com 
 Limitations: 
 		* Must Run PowerShell (or ISE) as an Administrator
        * UAC may get in the way depending on your settings
 ================================================================================
#>
# Run PowerShell as Administrator
param([switch]$Elevated)
function Check-Admin {
$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
if ((Check-Admin) -eq $false)  {
if ($elevated)
{
# could not elevate, quit
}
 
else {
 
Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
}
exit
}
#Set Remote Execution to: untrestricted
Set-ExecutionPolicy Unrestricted
Get-ExecutionPolicy

#Set Remote Execution to: untrestricted
Set-ExecutionPolicy RemoteSigned
Get-ExecutionPolicy

# You can also Run PowerShell in Different Execution Policy
powershell.exe /?
# -executionpolicy bypass
# -windowstyle hidden


#############################################
## Now Let's See how to do it with older versions of PowerShell
#############################################

#Set Remote Execution to: unrestricted
$MyPath = Get-Location   #Set the default value for path
$WritePath = $MyPath.ToString() + "\PowerShell-Execution-Unrestricted.reg"
Write-Host "Creating" $WritePath -ForegroundColor Green
$SaveFile = 'Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell]
"ExecutionPolicy"="Unrestricted"'
$fso = new-object -comobject scripting.filesystemobject
$file = $fso.CreateTextFile($WritePath,$true)  #will overwrite any existing file 
$file.write($SaveFile)
$file.close()

Write-Host "Finished" (Get-Date) -ForegroundColor Green
Regedit /S $WritePath
Get-ExecutionPolicy

#############################################
## Now RemoteSigned
#############################################
#Set Remote Execution to: RemoteSigned
$MyPath = Get-Location   #Set the default value for path
Write-Host $MyPath "will be used for creating registry files. You must have write permissions" -ForegroundColor Yellow
$WritePath = $MyPath.ToString() + "\PowerShell-Execution-RemoteSigned.reg"
Write-Host "Creating" $WritePath -ForegroundColor Green
$SaveFile = 'Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell]
"ExecutionPolicy"="RemoteSigned"'
$fso = new-object -comobject scripting.filesystemobject
$file = $fso.CreateTextFile($WritePath,$true)  #will overwrite any existing file 
$file.write($SaveFile)
$file.close()
Write-Host (Get-Date) -ForegroundColor Green
Regedit /S $WritePath
Get-ExecutionPolicy